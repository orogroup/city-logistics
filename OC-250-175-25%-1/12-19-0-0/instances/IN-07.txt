ONLINE REQUESTS:           32
TIME HORIZON:                  09 8 [09:00-16:59]
TIME BUCKETS IN HORIZON:   4   09 2 [09:00-10:59]     11 2 [11:00-12:59]     13 2 [13:00-14:59]     15 2 [15:00-16:59]
TIME BUCKETS FOR REQUESTS: 2   09 2 [09:00-10:59]     11 2 [11:00-12:59]
----------------------------------------------------------------------------------------------------------------------

NODE 		  VOL 		  TIME WINDOW 				  TIME REQUEST   TIME BUCKET
02200 		  009		  09 7 [09:00-16:59]		  09:05  		 1
05296 		  015		  09 7 [09:00-16:59]		  09:21  		 1
02083 		  021		  09 7 [09:00-16:59]		  09:21  		 1
02246 		  002		  09 7 [09:00-16:59]		  09:26  		 1
02622 		  020		  09 7 [09:00-16:59]		  09:35  		 1
02368 		  029		  09 7 [09:00-16:59]		  09:47  		 1
02266 		  001		  09 7 [09:00-16:59]		  09:53  		 1
02470 		  002		  09 7 [09:00-16:59]		  10:04  		 1
01090 		  001		  09 7 [09:00-16:59]		  10:08  		 1
05133 		  003		  09 7 [09:00-16:59]		  10:10  		 1
02396 		  003		  09 7 [09:00-16:59]		  10:10  		 1
02333 		  004		  09 7 [09:00-16:59]		  10:21  		 1
02281 		  004		  09 7 [09:00-16:59]		  10:39  		 1
02278 		  001		  09 7 [09:00-16:59]		  11:20  		 2
02483 		  003		  09 7 [09:00-16:59]		  11:30  		 2
02226 		  002		  09 7 [09:00-16:59]		  11:36  		 2
02456 		  001		  09 7 [09:00-16:59]		  11:44  		 2
50543 		  004		  09 7 [09:00-16:59]		  11:47  		 2
50555 		  001		  09 7 [09:00-16:59]		  11:54  		 2
02382 		  013		  09 7 [09:00-16:59]		  11:56  		 2
02198 		  014		  09 7 [09:00-16:59]		  11:57  		 2
02440 		  032		  09 7 [09:00-16:59]		  12:19  		 2
02475 		  003		  09 7 [09:00-16:59]		  12:23  		 2
02310 		  002		  09 7 [09:00-16:59]		  12:29  		 2
01097 		  022		  09 7 [09:00-16:59]		  12:31  		 2
02346 		  038		  09 7 [09:00-16:59]		  12:36  		 2
02200 		  009		  09 7 [09:00-16:59]		  12:38  		 2
02188 		  033		  09 7 [09:00-16:59]		  12:38  		 2
02038 		  010		  09 7 [09:00-16:59]		  12:40  		 2
02130 		  003		  09 7 [09:00-16:59]		  12:41  		 2
02330 		  001		  09 7 [09:00-16:59]		  12:52  		 2
05133 		  003		  09 7 [09:00-16:59]		  12:59  		 2
